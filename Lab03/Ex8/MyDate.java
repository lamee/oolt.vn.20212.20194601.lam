import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Scanner;

public class MyDate {
    private int day;
    private int month;
    private int year;
    private Date date;


    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public MyDate() {
    }

    public MyDate(String s) {
        formatDate(s);
    }

    private void formatDate(String s) {
        try {
            String test = "";
            if (s.contains("st")) {
                test = s.replace("st", "");
            } else if (s.contains("nd")) {
                test = s.replace("nd", "");

            } else if (s.contains("rd")) {
                test = s.replace("rd", "");

            } else {
                test = s.replace("th", "");
            }
            date = new SimpleDateFormat("MMMM dd yyyy").parse(test);
            System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        if (day < 1 || day > 31) {
            System.out.println("Day is not valid");
            return;
        }
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        if (month < 1 || month > 12) {
            System.out.println("Month is not valid");
        }
        this.month = month;
    }

    public int getYear() {

        return year;
    }

    public void setYear(int year) {

        this.year = year;
    }

    public void accept() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter your date: ");
        String date = sc.next();
        System.out.println("Please enter your month: ");
        String month = sc.next();
        System.out.println("Please enter your year: ");
        String year = sc.next();

        String result = month + " " + date + " " + year;

        formatDate(result);
    }

    public void print() {
        LocalDate date = LocalDate.now();
        System.out.print("Current date: ");
        System.out.print(date);
    }


}

