import java.util.ArrayList;

public class Order {
    public static final int MAX_NUMBERS_ORDERED = 10;

    private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];

    private int qtyOrdered = 0;
    public int getQtyOrdered() {
        return qtyOrdered;
    }

    public void setQtyOrdered(int qtyOrdered) {
        this.qtyOrdered = qtyOrdered;

    }
    public void addDigitalVideoDisc(DigitalVideoDisc disc) {
        int i = qtyOrdered;
        if (i < MAX_NUMBERS_ORDERED) {
            itemsOrdered[i] = disc;

            setQtyOrdered(qtyOrdered + 1);
            System.out.println("The disc has been added");
            if (qtyOrdered == MAX_NUMBERS_ORDERED - 1) {
                System.out.println("The order is almost full");
            }
        } else {
            System.out.println("Full!!");
            return;
        }
    }

    public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
        for (int i = 0; i < getQtyOrdered(); i++) {
            if(disc.getTitle().equals(itemsOrdered[i].getTitle())) {
                for (int j = i; j < getQtyOrdered() - 1; j++) {
                    itemsOrdered[j] = itemsOrdered[j + 1];
                }
                itemsOrdered[qtyOrdered - 1] = new DigitalVideoDisc();
                setQtyOrdered(qtyOrdered - 1);
                return;
            }
            System.out.println("Can't find disc want to remove");
        }
    }

    public float totalCost() {
        float sum = 0.00f;
        try {
            for(int i = 0; i < getQtyOrdered(); i++) {
                float a = itemsOrdered[i].getCost();
                sum += a;
            }
            return sum;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public DigitalVideoDisc[] getItemsOrdered() {
        return itemsOrdered;
    }
}

