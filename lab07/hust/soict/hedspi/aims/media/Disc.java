package hust.soict.hedspi.aims.media;

public class Disc extends Media {
    protected int length;
    private String director;

    public int getLength() {
        return length;
    }
    public void setLength(int length) {
        this.length = length;
    }
    public String getDirector() {
        return director;
    }
    public void setDirector(String director) {
        this.director = director;
    }

    public Disc(String director, int length){
        super(id, title, category, cost);
        this.director = director;
        this.length = length;
    }

    public Disc(String id, String title, float cost, int length, String director) {
        super(id, title, cost);
        this.length = length;
        this.director = director;
    }
    public Disc(String id, String title, String category, float cost) {
        super(id, title, category, cost);
    }

    
    
}
