public class AimsProject {
    public static void main(String[] args) {
        Order order = new Order();

        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King","Animation","Roger Allers",87,19.95f);
        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star War","Science Fictiopn","George Lucas",124,24.95f);
        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin","Animation","John Musker",90,18.99f);
        order.addDigitalVideoDisc(dvd1);
        order.addDigitalVideoDisc(dvd2);
        order.addDigitalVideoDisc(dvd3);
        order.addDigitalVideoDisc(dvd1);

        order.removeDigitalVideoDisc(dvd1);

        System.out.println("Total Cost is: ");
        System.out.println(order.totalCost());
    }
}
