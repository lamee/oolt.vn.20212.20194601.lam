package hust.soict.hedspi.media;
import java.lang.Object
public class Media {
  protected String tilte;
  protected String category;
  protected float cost;

    public Media() {
    }

    public String getTilte() {
        return tilte;
    }

    public void setTilte(String tilte) {
        this.tilte = tilte;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }
}
