package hust.soict.hedspi.media;
import java.lang.Object
import java.util.ArrayList;

public class Book extends Media {
    private String title;
    private String category;
    private float cost;
    private List<String> authors = new ArrayList<>();

    public List<String> getAuthors() {
        return authors;
    }

    public Book(List<String> authors) {
        this.authors = authors;
    }

    public addAuthor(String authorName){
        if(!(authors.contains(authorName))){
            authors.add(authorName);
        }
    }
    public removeAuthor(String authorName){
        if(!(authors.contains(authorName))){
            authors.remove(authorName);
        }
    }
}
