package hust.soict.hedspi.aims.order;
import java.util.Arrays;
import java.util.Random;

import hust.soict.hedspi.aims.utils.MyDate;
import hust.soict.hedspi.media.DigitalVideoDisc;
import hust.soict.hedspi.media.Media;

import java.util.Date;

public class Order {
    public static final int MAX_NUMBERS_ORDERED = 10;
    private int qtyOrdered = 0;
    private MyDate orderDate;
    private static int nbOrders = 0;
    public static final int MAX_LIMITED_ORDERS = 5;
    private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];

    public Order() {
        if (nbOrders >= MAX_LIMITED_ORDERS) {
            System.out.println("Reached number orders limit.");
            return;
        }
        Date today = new Date();
        this.orderDate = new MyDate(today.getDate(), today.getMonth() + 1, today.getYear() + 1900);
        nbOrders++;
    }

    private int arrayLength (DigitalVideoDisc [] array) {
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != null) {
                count++;
            }
        }
        return count;
    }

    public void addDigitalVideoDisc(DigitalVideoDisc disc) {
        if(qtyOrdered >= MAX_NUMBERS_ORDERED) {
            System.out.println("The order is full.");
            return;
        }

        if (qtyOrdered == MAX_NUMBERS_ORDERED - 1) {
            System.out.println("The order is almost full.");
        }

        itemsOrdered[qtyOrdered] = disc;
        qtyOrdered++;
        System.out.println("Order " + disc.getTitle() + " successfully.");
        return;
    }
    public float totalCost() {
        float total = 0;
        Media mediaItem;
        java.util.Iterator iter = Arrays.stream(itemsOrdered).iterator();
        while(iter.hasNext()){
            mediaItem = (Media)(iter.next());
            total += mediaItem.getCost();
        }

        return total;
    }

    public void print() {
        System.out.println("Date: " + orderDate.getDay() + "/" + orderDate.getMonth() + "/" + orderDate.getYear() + ".");
        System.out.println("Ordered Items:");
        for (int i = 0; i < qtyOrdered; i++) {
            System.out.println((i + 1) + ". DVD - " + itemsOrdered[i].getTitle() + " - " + itemsOrdered[i].getCategory() + " - " + itemsOrdered[i].getDirector() + " - " + itemsOrdered[i].getLength() + ": " + itemsOrdered[i].getCost() + "$");
        }
        System.out.println("Total cost: " + totalCost());
    }

    public Media getALuckyItem(){
        Random rd = new Random();
        int luckyNumber = rd.nextInt(this.itemsOrdered.size());
        itemsOrdered.get(luckyNumber).setCost(0);
        return itemsOrdered.get(luckyNumber);
            }


    }
    public void addMedia(Media media){
        if(!this.itemsOrdered.contains(media))
            this.itemsOrdered.add(Media);
    }
    public void removeMedia(Media media){
        if(this.itemsOrdered.contains(media))
            this.itemsOrdered.remove(media);

    }
}
