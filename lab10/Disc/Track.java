package Disc;

import hust.soict.hedspi.aims.exceptions.PlayerException;
import hust.soict.hedspi.aims.media.disc.Playable;

public class Track  implements Playable{
    private String title;
    private int length;
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public int getLength() {
        return length;
    }
    public void setLength(int length) {
        this.length = length;
    }
    public Track(String title, int length) {
        this.title = title;
        this.length = length;
    }
    public Track(){

    }
    @Override
    public void play() throws PlayerException {
        if(this.getLength() <= 0){
            System.err.println("ERROR: DVD length is 0");
            throw (new PlayerException());
        }

        System.out.println("Playing DigitalVideoDisc: " + this.getTitle());
        System.out.println("DigitalVideoDisc length:  " + this.getLength());
    }

    public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if( obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Track other = (Track) obj;
		if (length != other.length){
			return false;
		}
		if (title == null) {
			if(other.title != null)
				return false;
		} else if(!title.equals(other.title))
			return false;
		return true;
	}
	
	public int compareTo(Track track) {
		if(this.title.compareToIgnoreCase(track.title) < 0)
			return -1;
		else if(this.title.compareToIgnoreCase(track.title) == 0)
			return 0;
		else return 1;
	}
    
}
