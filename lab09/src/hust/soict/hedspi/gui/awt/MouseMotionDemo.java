package hust.soict.hedspi.gui.awt;

import java.awt.*;
import java.awt.event.*;

public class MouseMotionDemo extends Frame {
    private TextField tfMouseClickX;
    private TextField tfMouseClickY;
    private TextField tfMousePositionX;
    private TextField tfMousePositionY;

    public MouseMotionDemo(){
        setLayout(new FlowLayout());

        add(new Label("X-Click: "));
        tfMouseClickX = new TextField(10);
        tfMouseClickX.setEditable(false);
        add(tfMouseClickX);
        add(new Label("Y-Click: "));
        tfMouseClickY = new TextField(10);
        tfMouseClickY.setEditable(false);
        add(tfMouseClickY);

        add(new Label("X-Position: "));
        tfMousePositionX = new TextField(10);
        tfMousePositionX.setEditable(false);
        add(tfMousePositionX);
        add(new Label("Y-Position: "));
        tfMousePositionY = new TextField(10);
        tfMousePositionY.setEditable(false);
        add(tfMousePositionY);

        MyMouseListener listener = new MyMouseListener();
        addMouseListener(listener);
        addMouseMotionListener(listener);

        setTitle("MouseMotion Demo");
        setSize(400, 120);
        setVisible(true);
    }

    public static void main(String[] args) {
        new MouseMotionDemo();
    }

    private class MyMouseListener implements MouseListener, MouseMotionListener {

        @Override
        public void mouseClicked(MouseEvent e) {
            tfMouseClickX.setText(e.getX() + "");
            tfMouseClickY.setText(e.getY() + "");
            
        }

        @Override
        public void mousePressed(MouseEvent e) {
           
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            
        }

        @Override
        public void mouseEntered(MouseEvent e) {
           
        }

        @Override
        public void mouseExited(MouseEvent e) {
            
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            tfMousePositionX.setText(e.getX() + "");
            tfMousePositionY.setText(e.getY() + "");
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            
        }
        
    }
}
