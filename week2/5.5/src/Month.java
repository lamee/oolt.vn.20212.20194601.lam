public class Month {
    private String name;
    private String abbreviation;
    private String inThreeLetter;
    private int number;
    private int commonDays;
    private int leapDays;

    public Month(String name, String abbreviation, String inThreeLetter, int number, int commonDays, int leapDays) {
        this.name = name;
        this.abbreviation = abbreviation;
        this.inThreeLetter = inThreeLetter;
        this.number = number;
        this.commonDays = commonDays;
        this.leapDays = leapDays;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getInThreeLetter() {
        return inThreeLetter;
    }

    public void setInThreeLetter(String inThreeLetter) {
        this.inThreeLetter = inThreeLetter;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getCommonDays() {
        return commonDays;
    }

    public void setCommonDays(int commonDays) {
        this.commonDays = commonDays;
    }

    public int getLeapDays() {
        return leapDays;
    }

    public void setLeapDays(int leapDays) {
        this.leapDays = leapDays;
    }
}
