import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class DaysInMonth {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter a month: ");
        String input = sc.nextLine();
        System.out.print("Enter a year: ");
        int year = sc.nextInt();
        int result = 0;
        for (Month month : Data.data) {
            if (input.equals(month.getName()) || input.equals(month.getAbbreviation()) || input.equals(month.getInThreeLetter()) || input.equals(String.valueOf(month.getNumber()))) {
                if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0)))
                    result = month.getLeapDays();
                else result = month.getCommonDays();
                System.out.println("Number of days in this month: " + result);
                break;
            }
        }
        if (result == 0) System.out.println("Unexpected Error");
    }
}
