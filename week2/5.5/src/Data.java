import java.util.ArrayList;
import java.util.Arrays;

public class Data {
    public static ArrayList<Month> data = new ArrayList<>(Arrays.asList(
            new Month("January", "Jan.", "Jan", 1, 31, 31),
            new Month("February", "Feb.", "Feb", 2, 28, 29),
            new Month("March", "Mar.", "Mar", 3, 31, 31),
            new Month("April", ".", "Apr", 4, 30, 30),
            new Month("May", "May", "May", 5, 31, 31),
            new Month("June", "June", "Jun", 6, 30, 30),
            new Month("July", "July", "Jul", 7, 31, 31),
            new Month("August", "Aug.", "Aug", 8, 31, 31),
            new Month("September", "Sept.", "Sept", 9, 30, 30),
            new Month("October", "Oct.", "Oct", 10, 31, 31),
            new Month("November", "Nov.", "Nov", 11, 30, 30),
            new Month("December", "Dec.", "Dec", 12, 31, 31)));
}
