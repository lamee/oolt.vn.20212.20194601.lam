import java.util.ArrayList;
import java.util.Scanner;

public class Sort {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int length = sc.nextInt();
        int[] arrayList = new int[length];
        for (int i = 0; i < length; i++) {
            arrayList[i] = sc.nextInt();
        }
        int[] result = sort(arrayList, length);
        System.out.println("Mang sau khi sap xep:");
        for (int i : result) {

            System.out.print(i + " ");
        }
        System.out.println();
        System.out.println("Tong cua mang:  " + sum(arrayList,length));
        System.out.print("Trung binh cac gia tri trong mang: " );
        average(arrayList,length);

    }

    public static int[] sort(int[] a, int n) {
        for (int i = 0; i < n - 1; i++) {
            for (int j = n - 1; j > i; j--) {
                if (a[j] < a[j - 1]) {
                    int tmp = a[j];
                    a[j] = a[j - 1];
                    a[j - 1] = tmp;
                }
            }
        }
        return a;
    }

    public static int sum(int[] a, int n) {
        int sum = 0;
        for (int i = 0; i < n; i++) {
            sum += a[i];
        }
        return sum;
    }

    public static void average(int[] a, int n) {
        if(n == 0) System.out.println("Length of the array is invalid");
        else System.out.println(sum(a, n) * 1.0 / n);
    }
}
