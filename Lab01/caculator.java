import javax.swing.*;


public class Caculator {

    public static void main(String[] args) {
    
        try {
            String num1, num2;
            num1 = JOptionPane.showInputDialog("Please enter your first number:");
            num2 = JOptionPane.showInputDialog("Please enter your second number:");

            double a = Double.parseDouble(num1);

            double b = Double.parseDouble(num2);

            JOptionPane.showMessageDialog(null, "Hi your result is : \nSum: " + MathSum(a, b)
                    + "\nDifference: " + MathDiff(a, b) + "\nProduct: " + MathPro(a, b) + "\nQuotient: " + MathQuo(a, b));
            System.exit(0);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.toString());
        }
    }


    public static double MathSum(double num1, double num2) {
        return num1 + num2;
    }


    public static double MathDiff(double num1, double num2) {
        return num1 - num2;
    }


    public static double MathPro(double num1, double num2) {
        return num1 * num2;
    }


    public static double MathQuo(double num1, double num2) {
        return num1 / num2;
    }
}
