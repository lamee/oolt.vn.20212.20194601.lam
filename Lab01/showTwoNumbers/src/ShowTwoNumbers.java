import javax.swing.JOtionPane;

public class ShowTwoNumbers {
    public static void main(String[] args) {
        String strNum1, strNum2;
        String strNotification = "You've just entered: ";
        strNum1 = JOtionPane.showInputDialog(null, "Please input the first number", JOtionPane.INFORMATION_MESSAGE);
        strNotification += strNum1 + " and ";
        strNum2 = JOtionPane.showInputDialog(null, "Please input the second number: ", "Input the second number",
                JOtionPane.INFORMATION_MESSAGE);
        strNotification += strNum2;
        JOtionPane.showMessageDialog(null, strNotification, "Show two numbers", JOtionPane.INFORMATION_MESSAGE);
        System.exit(0);
    }
}
