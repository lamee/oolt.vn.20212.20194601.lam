import javax.swing.*;

public class SolveMath {
    public static void main(String[] args) {
        Object[] possibleValues = {"The first-degree equation",
                "The second-degree", "A system of first-degree"};
        Object selectedValue = JOptionPane.showInputDialog(null, "Choose one", "input",
                JOptionPane.INFORMATION_MESSAGE, null, possibleValues, possibleValues[0]);
        switch (selectedValue.toString()) {
            case "The first-degree equation":
                String strNum1, strNum2;
                JOptionPane.showMessageDialog(null, "Your equation is: ax + b = 0");
                strNum1 = JOptionPane.showInputDialog(null, "Please enter your a: ");
                strNum2 = JOptionPane.showInputDialog(null, "Please enter your b: ");
                double num1 = Double.parseDouble(strNum1);
                double num2 = Double.parseDouble(strNum2);
                JOptionPane.showMessageDialog(null, "Your result is: " + (-num2 * 1.0 / num1));
                break;
            case "The second-degree":
                JOptionPane.showMessageDialog(null, "Your equation is: ax^2 + bx +c = 0");
                String strFir, strSec, strThr;
                strFir = JOptionPane.showInputDialog(null, "Please enter your a: ");
                strSec = JOptionPane.showInputDialog(null, "Please enter your b: ");
                strThr = JOptionPane.showInputDialog(null, "Please enter your c: ");
                double Fir = Double.parseDouble(strFir);
                double Sec = Double.parseDouble(strSec);
                double Thr = Double.parseDouble(strThr);
                double denta = Sec * Sec - 4.0 * Fir * Thr;
                if (denta > 0.0) {
                    double r1 = (-Sec + Math.pow(denta, 0.5)) / (2.0 * Fir);
                    double r2 = (-Sec - Math.pow(denta, 0.5)) / (2.0 * Fir);
                    JOptionPane.showMessageDialog(null, "The roots are: " + r1 + " and " + r2);

                } else if (denta == 0) {
                    double r1 = -Sec / (2.0 * Fir);
                    JOptionPane.showMessageDialog(null, "The root is: " + r1);
                }

                else{
                    JOptionPane.showMessageDialog(null,"Roots are not real");
                }
                break;
            case "A system of first-degree":
                JOptionPane.showMessageDialog(null,"The system is: a_11*x1 + a_12* x2 = b1 \n a_21*x1 + a_22*x2 = b2");
                String stra11, stra12, strb1, stra21, stra22,strb2;
                stra11 = JOptionPane.showInputDialog(null,"Enter a11: ");
                stra12 = JOptionPane.showInputDialog(null,"Enter a11: ");
                strb1 = JOptionPane.showInputDialog(null,"Enter a11: ");
                stra21 = JOptionPane.showInputDialog(null,"Enter a11: ");
                stra22 = JOptionPane.showInputDialog(null,"Enter a11: ");
                strb2 = JOptionPane.showInputDialog(null,"Enter a11: ");
                double a11 = Double.parseDouble(stra11);
                double a12 = Double.parseDouble(stra12);
                double b1 = Double.parseDouble(strb1);
                double a21 = Double.parseDouble(stra21);
                double a22 = Double.parseDouble(stra22);
                double b2 = Double.parseDouble(strb2);
                double D = a11*a22 - a21*a12;
                double D1 = b1*a22 - b2*a12;
                double D2 = a11*b2 - a21*b1;
                if(D != 0 ){
                    JOptionPane.showMessageDialog(null,"Your result (x1, x2) = ("  + D1/D + " , " + D2/D + " )");
                }
                else if(D== D1 && D1==D2 && D== 0){
                    JOptionPane.showMessageDialog(null,"The system has infinitely many solutions!");

                }
                else{
                    JOptionPane.showMessageDialog(null,"The system has no solution!");
                }



                break;
            default:
                System.out.println("default");
                break;
        }
    }


}
